  <?php
   include_once 'conexao.php';
        if(isset($_POST['cadastrar']) && $_POST['cadastrar'] == 'sim') {
            $novos_campos = array();
            $campos_post = $_POST['campos'];
            $respostas = array();

            foreach($campos_post as $indice => $valor){
                $novos_campos[$valor['name']] = $valor['value'];
        
            }if(strlen($novos_campos['cpf']) <> 11){
                $respostas['erro'] = 'sim';
                $respostas['getErro'] = 'Insira corretamente o cpf...';
                        
            }elseif(strlen($novos_campos['telefone']) <> 11){
                $respostas['erro'] = 'sim';
                $respostas['getErro'] = 'Insira seu telefone corretamente...';
                
            }elseif(!strstr($novos_campos['email'], '@')){
                $respostas['erro'] = 'sim';
                $respostas['getErro'] = 'E-mail invalido...';
                
            }else{
                $respostas['erro'] = 'nao';
                $inserir_banco = $con->prepare("INSERT INTO pesquisa_satisfacao(nome, cpf, telefone, email, nota, nota_comentario, 
                    relacao_hospital, servicos, avaliacao_1, avaliacao_2, avaliacao_3, final_comunicar, sugestao) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
                $inserir_banco->bindParam(1, $novos_campos['nome']);
                $inserir_banco->bindParam(2, $novos_campos['cpf']);
                $inserir_banco->bindParam(3, $novos_campos['telefone']);
                $inserir_banco->bindParam(4, $novos_campos['email']);
                $inserir_banco->bindParam(5, $novos_campos['nota']);
                $inserir_banco->bindParam(6, $novos_campos['nota_comentario']);
                $inserir_banco->bindParam(7, $novos_campos['relacao_hospital']);
                $inserir_banco->bindParam(8, $novos_campos['servicos']);
                $inserir_banco->bindParam(9, $novos_campos['avaliacao_1']);
                $inserir_banco->bindParam(10, $novos_campos['avaliacao_2']);
                $inserir_banco->bindParam(11, $novos_campos['avaliacao_3']);
                $inserir_banco->bindParam(12, $novos_campos['final_comunicar']);
                $inserir_banco->bindParam(13, $novos_campos['comente']);
                    
                $array_sql = array(
                        $novos_campos['nome'],
                        $novos_campos['cpf'],
                        $novos_campos['telefone'],
                        $novos_campos['email'],
                        $novos_campos['nota'],
                        $novos_campos['nota_comentario'],
                        $novos_campos['relacao_hospital'],
                        $novos_campos['servicos'],
                        $novos_campos['avaliacao_1'],
                        $novos_campos['avaliacao_2'],
                        $novos_campos['avaliacao_3'],
                        $novos_campos['final_comunicar'],
                        $novos_campos['comente']
                            
                    );
                $inseriu = $inserir_banco->execute();
                if($inseriu){
                    $respostas['msg'] = 'inserido corretamente...';
                }else{
                    $respostas['erro'] = 'sim';
                    $respostas['getErro'] = 'Por favor, tente mais tarde...';
                }
            }
            echo json_encode($respostas);
        }
?>

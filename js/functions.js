$(function(){
    var atual_fs, next_fs, prev_fs;
    var formulario = $('form[name=formulario]');
    
    function next(elem){
        atual_fs = $(elem).parent();
        next_fs = $(elem).parent().next();
        
                
$('#progress li').eq($('fieldset').index(next_fs)).addClass('ativo');
        atual_fs.hide(800);
        next_fs.show(800);
    }
    
     $('.prev').click(function(){
        atual_fs = $(this).parent();
        prev_fs = $(this).parent().prev();
        
                
$('#progress li').eq($('fieldset').index(atual_fs)).removeClass('ativo');
        atual_fs.hide(800);
        prev_fs.show(800);
    });      
         $('input[name=next1]').click(function(){
             var array = formulario.serializeArray();
             if(array[0].value == '' || array[1].value == '' || array[2].value == '' || array[3].value == '' ){
                 $('.resp').html('<div class="erros"><p>Preencha todos os campos corretamentes...</p></div>');
             }else{
                 $('.resp').html('');
                 next($(this));
             }
         });
     $('input[name=next2]').click(function(){
             var array = formulario.serializeArray();
         if (array[4].value == '' || array[5].value == '' || array[6].value == '' || array[7].value == '') {
                 $('.resp').html('<div class="erros"><p>Ops!! Esqueceu de preencher algum campo...</p></div>');
             }else{
                 $('.resp').html('');
                 next($(this));
             }
         });
    $('input[name=next3]').click(function(){
            var array = formulario.serializeArray();
            if(array[8].value == '' || array[9].value == '' || array[10].value == '' ){
                $('.resp').html('<div class="erros"><p>Ops!! Esqueceu de preencher algum campo...</p></div>');
            }else{
                $('.resp').html('');
                next($(this));
            }
        });
        $('input[type=submit]').click(function(evento){
            var array = formulario.serializeArray();
            if(array[11].value == '' || array[12].value == ''){
                $('.resp').html('<div class="erros"><p>Ops! Esqueceu de preencher algum campo...</p></div>');
            
            }else{
            $.ajax({
                method: 'post',
                url: 'cadastrar.php',
                cache: false,
                data: {cadastrar: 'sim', campos: array},
                dataType: 'json',
                beforeSend: function(){
                    $('.resp').html('<div class="erros"><p>Aguarde enquanto processamos...</p></div>');  
                },
                success: function(valor){
                    console.log(valor)
                    if(valor.erro == 'sim'){
                    $('.resp').html('<div class="erros"><p>'+valor.getErro+'</p></div>');   
                        
                        }else{
                            $('.resp').html('<div class="ok"><p>'+valor.msg+'</p></div>');                    
                    }
                },
                error: function(msg) {
                    console.log(msg)
                }
            });
        }
            evento.preventDefault();
    });
});
